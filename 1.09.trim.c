#include <stdio.h>

int main() {

    int spaces = 0;
    int c = getchar();

    while ( c != EOF ) {

        if ( c != ' ' ) {
            spaces = 0;
            putchar(c);
        } else  {
            ++spaces;
            if ( spaces == 1 )
                putchar(c);
        }
        c = getchar();
    }

    return 0;
}
